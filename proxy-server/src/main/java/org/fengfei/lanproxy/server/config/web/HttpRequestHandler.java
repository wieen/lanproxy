package org.fengfei.lanproxy.server.config.web;

import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.HttpHeaders.Names;
import io.netty.handler.ssl.SslHandler;
import io.netty.handler.stream.ChunkedNioFile;
import org.fengfei.lanproxy.common.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.RandomAccessFile;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

public class HttpRequestHandler extends SimpleChannelInboundHandler<FullHttpRequest> {

    private static final String PAGE_FOLDER = System.getProperty("app.home", System.getProperty("user.dir"))
            + "/webpages";

    private static Logger logger = LoggerFactory.getLogger(HttpRequestHandler.class);


    //这里有当前服务的标志头，导致会很容易监测到，换成一个随机的header,这样就无法进行header检测
    //private static final String SERVER_VS = "LPS-0.11";

    static List<String> languageList = Arrays.<String>asList(new String[] { "ASP.NET", "ASP", "PHP/5.4.27","PHP/7.1.0","PHP/7.2.0","PHP/7.3.0", "JScript",
            "VB.NET", "VBScript", "CGI", "Python", "Perl", "JAVA", "ELanguage" });

    private static List<String> serverList = Arrays.<String>asList(new String[]{"Microsoft-IIS/10.0", "Microsoft-IIS/9.0",
            "Microsoft-IIS/9.5", "Microsoft-IIS/3.0", "Microsoft-IIS/3.5", "Microsoft-IIS/2.0", "Microsoft-IIS/2.5",
            "Caddy-Server/0.9", "Caddy-Server/1.0", "Caddy-Server/1.1", "Hacker-Server/2.0", "Hacker-Server/3.0",
            "Hacker-Server/4.0", "Hacker-Server/8.0", "Hacker-Server/9.0", "Hacker-Server/2.5", "Hacker-Server/3.5",
            "Hacker-Server/4.5", "Hacker-Server/8.5", "Hacker-Server/9.5", "ASP-Server/2.5", "ASP-Server/3.5",
            "ASP-Server/4.5", "ASP-Server/5.5", "Xampp-Server/2.5", "Xampp-Server/3.5", "Xampp-Server/5.5",
            "Xampp-Server/6.0", "Xampp-Server/8.5", "nginx/1.18.1", "Server: Apache/2.4.1"
    });


    public static Integer getRanDom(int start, int end) {
        return (int) (Math.random() * (end - start + 1)) + start;
    }

    public String getServerName() {
        return serverList.get(getRanDom(0, serverList.size() - 1));
    }

    public String getPowerBy() {
        return languageList.get(getRanDom(0, languageList.size() - 1));
    }


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest request) throws Exception {

        // GET返回页面；POST请求接口
        if (request.getMethod() != HttpMethod.POST) {
            outputPages(ctx, request);
            return;
        }

        ResponseInfo responseInfo = ApiRoute.run(request);

        // 错误码规则：除100取整为http状态码
        outputContent(ctx, request, responseInfo.getCode() / 100, JsonUtil.object2json(responseInfo),
                "Application/json;charset=utf-8");
    }

    private void outputContent(ChannelHandlerContext ctx, FullHttpRequest request, int code, String content,
                               String mimeType) {
        FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.valueOf(code),
                Unpooled.wrappedBuffer(content.getBytes(Charset.forName("UTF-8"))));
        response.headers().set(Names.CONTENT_TYPE, mimeType);
        response.headers().set(Names.CONTENT_LENGTH, response.content().readableBytes());
        response.headers().set("X-Powered-By", getPowerBy());
        response.headers().set(Names.SERVER, getServerName());
        ChannelFuture future = ctx.writeAndFlush(response);
        if (!HttpHeaders.isKeepAlive(request)) {
            future.addListener(ChannelFutureListener.CLOSE);
        }
    }

    public String urlHandler(String path) {
        path = path.replaceAll("\\.\\./", "/");
        path = path.replaceAll("\\./", "/");
        return path;
    }

    /**
     * 输出静态资源数据
     *
     * @param ctx
     * @param request
     * @throws Exception
     */
    private void outputPages(ChannelHandlerContext ctx, FullHttpRequest request) throws Exception {
        HttpResponseStatus status = HttpResponseStatus.OK;
        URI uri = new URI(request.getUri());
        String uriPath = uri.getPath();
        uriPath = uriPath.equals("/") ? "/index.html" : uriPath;
        String path = PAGE_FOLDER + uriPath;

        path = urlHandler(path);
        File rfile = new File(path);
        if (rfile.isDirectory()) {
            path = path + "/index.html";
            rfile = new File(path);
        }

        if (!rfile.exists()) {
            status = HttpResponseStatus.NOT_FOUND;
            outputContent(ctx, request, status.code(), status.toString(), "text/html");
            return;
        }

        if (HttpHeaders.is100ContinueExpected(request)) {
            send100Continue(ctx);
        }

        String mimeType = MimeType.getMimeType(MimeType.parseSuffix(path));
        long length = 0;
        RandomAccessFile raf = null;
        try {
            raf = new RandomAccessFile(rfile, "r");
            length = raf.length();
        } finally {
            if (length < 0 && raf != null) {
                raf.close();
            }
        }

        HttpResponse response = new DefaultHttpResponse(request.getProtocolVersion(), status);
        response.headers().set(HttpHeaders.Names.CONTENT_TYPE, mimeType);
        boolean keepAlive = HttpHeaders.isKeepAlive(request);
        if (keepAlive) {
            response.headers().set(HttpHeaders.Names.CONTENT_LENGTH, length);
            response.headers().set(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
        }

        response.headers().set("X-Powered-By", getPowerBy());
        response.headers().set(Names.SERVER, getServerName());
        ctx.write(response);

        if (ctx.pipeline().get(SslHandler.class) == null) {
            ctx.write(new DefaultFileRegion(raf.getChannel(), 0, length));
        } else {
            ctx.write(new ChunkedNioFile(raf.getChannel()));
        }

        ChannelFuture future = ctx.writeAndFlush(LastHttpContent.EMPTY_LAST_CONTENT);
        if (!keepAlive) {
            future.addListener(ChannelFutureListener.CLOSE);
        }
    }

    private static void send100Continue(ChannelHandlerContext ctx) {
        FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.CONTINUE);
        ctx.writeAndFlush(response);
    }

}
